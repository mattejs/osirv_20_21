import numpy as np
import cv2
import matplotlib.pyplot as plt
import os

pathWrite = '/home/matej/osirv_20_21/lab3/report/zad1'
pathRead = '/home/matej/osirv_20_21/lab3/slike'

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread(os.path.join(pathRead,'goldhill.bmp'), 0)

for percent in range(5,21,5):
    showhist(salt_n_pepper_noise(img, percent))    
    cv2.imwrite(os.path.join(pathWrite,"goldhill_SaltAndPepper" + str(percent) + ".bmp"), salt_n_pepper_noise(img, percent))

for i in range(20,61,20):
    showhist(uniform_noise(img, -i, i))   
    cv2.imwrite(os.path.join(pathWrite,"goldhill_Uniform" + str(i) + ".bmp"), uniform_noise(img, -i, i))

sigmas = [1, 5, 10, 20, 40, 60]

for sigma in sigmas:
    showhist(gaussian_noise(img, 0, sigma))
    cv2.imwrite(os.path.join(pathWrite,"goldhill_Gaussian" + str(sigma) + ".bmp"), gaussian_noise(img, 0, sigma))


