# Problem 1 - Noise

1. Na sliku po izboru dodajte sva tri tipa šuma s parametrima:

  - Gaussian: $`  \mu = 0  `$,   $`  \sigma = \[ 1, 5, 10, 20, 40, 60 \]  `$
  - Uniform: granice $`  = [(-20,20), (-40,40), (-60,60)]  `$
  - Salt and pepper: postotak slike $`  =  [ 5\%, 10\%, 15\%, 20% ]  `$
  
  **Snimite stvorene slike.** Prikažite histograme za svaku od slika. Probajte
  uočiti utjecaj šuma u histogramu slike.

```python
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os

pathWrite = '/home/matej/osirv_20_21/lab3/report/zad1'
pathRead = '/home/matej/osirv_20_21/lab3/slike'

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread(os.path.join(pathRead,'goldhill.bmp'), 0)

for percent in range(5,21,5):
    showhist(salt_n_pepper_noise(img, percent))    
    cv2.imwrite(os.path.join(pathWrite,"goldhill_SaltAndPepper" + str(percent) + ".bmp"), salt_n_pepper_noise(img, percent))

for i in range(20,61,20):
    showhist(uniform_noise(img, -i, i))   
    cv2.imwrite(os.path.join(pathWrite,"goldhill_Uniform" + str(i) + ".bmp"), uniform_noise(img, -i, i))

sigmas = [1, 5, 10, 20, 40, 60]

for sigma in sigmas:
    showhist(gaussian_noise(img, 0, sigma))
    cv2.imwrite(os.path.join(pathWrite,"goldhill_Gaussian" + str(sigma) + ".bmp"), gaussian_noise(img, 0, sigma))
```

![goldhill_Gaussian1](zad1/goldhill_Gaussian1.bmp)
![goldhill_Gaussian5](zad1/goldhill_Gaussian5.bmp)
![goldhill_Gaussian10](zad1/goldhill_Gaussian10.bmp)
![goldhill_Gaussian12](zad1/goldhill_Gaussian12.bmp)
![goldhill_Gaussian20](zad1/goldhill_Gaussian20.bmp)
![goldhill_Gaussian40](zad1/goldhill_Gaussian40.bmp)
![goldhill_Gaussian60](zad1/goldhill_Gaussian60.bmp)

![goldhill_SaltAndPepper5](zad1/goldhill_SaltAndPepper5.bmp)
![goldhill_SaltAndPepper10](zad1/goldhill_SaltAndPepper10.bmp)
![goldhill_SaltAndPepper15](zad1/goldhill_SaltAndPepper15.bmp)
![goldhill_SaltAndPepper20](zad1/goldhill_SaltAndPepper20.bmp)

![goldhill_Uniform20](zad1/goldhill_Uniform20.bmp)
![goldhill_Uniform40](zad1/goldhill_Uniform40.bmp)
![goldhill_Uniform60](zad1/goldhill_Uniform60.bmp)


# Problem 2 - Thresholding

1. Compare the above thresholding methods on images `boats`, `baboon`
and `airplane`. Try using different thresholds for images.

```python
import cv2
import numpy as np
from matplotlib import pyplot as plt
import os

pathWrite = '/home/matej/osirv_20_21/lab3/report/zad2'
pathRead = '/home/matej/osirv_20_21/lab3/slike'

imgs = ['boats.bmp','baboon.bmp','airplane.bmp']

for img in imgs:
    image = cv2.imread(os.path.join(pathRead,img), 0)    
    
    ret,thresh1 = cv2.threshold(image,127,255,cv2.THRESH_BINARY)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_BINARY.png"), thresh1)
    ret,thresh2 = cv2.threshold(image,127,255,cv2.THRESH_BINARY_INV)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_BINARY_INV.png"), thresh2)
    ret,thresh3 = cv2.threshold(image,127,255,cv2.THRESH_TRUNC)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_TRUNC.png"), thresh3)
    ret,thresh4 = cv2.threshold(image,127,255,cv2.THRESH_TOZERO)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_TOZERO.png"), thresh4)
    ret,thresh5 = cv2.threshold(image,127,255,cv2.THRESH_TOZERO_INV)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_TOZERO_INV.png"), thresh5)
     
    th2 = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(os.path.join(pathWrite, img.split('.')[0] + "_ADAPTIVE_THRESH_MEAN_C.png"), th2)
    th3 = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(os.path.join(pathWrite, img.split('.')[0] + "_ADAPTIVE_THRESH_GAUSSIAN_C.png"),th3)
    
    ret2,th4 = cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    cv2.imwrite(os.path.join(pathWrite, img.split('.')[0] + "_THRESH_BINARY+THRESH_OTSU.png"), th4)
```

### Simple Thresholding

![airplane_THRESH_BINARY](zad2/airplane_THRESH_BINARY.png)
![airplane_THRESH_BINARY_INV](zad2/airplane_THRESH_BINARY_INV.png)
![airplane_THRESH_TOZERO](zad2/airplane_THRESH_TOZERO.png)
![airplane_THRESH_TOZERO_INV](zad2/airplane_THRESH_TOZERO_INV.png)
![airplane_THRESH_TRUNC](zad2/airplane_THRESH_TRUNC.png)

![baboon_THRESH_BINARY](zad2/baboon_THRESH_BINARY.png)
![baboon_THRESH_BINARY_INV](zad2/baboon_THRESH_BINARY_INV.png)
![baboon_THRESH_TOZERO](zad2/baboon_THRESH_TOZERO.png)
![baboon_THRESH_TOZERO_INV](zad2/baboon_THRESH_TOZERO_INV.png)
![baboon_THRESH_TRUNC](zad2/baboon_THRESH_TRUNC.png)

![boats_THRESH_BINARY](zad2/boats_THRESH_BINARY.png)
![boats_THRESH_BINARY_INV](zad2/boats_THRESH_BINARY_INV.png)
![boats_THRESH_TOZERO](zad2/boats_THRESH_TOZERO.png)
![boats_THRESH_TOZERO_INV](zad2/boats_THRESH_TOZERO_INV.png)
![boats_THRESH_TRUNC](zad2/boats_THRESH_TRUNC.png)

### Adaptive Thresholding

![airplane_ADAPTIVE_THRESH_GAUSSIAN_C](zad2/airplane_ADAPTIVE_THRESH_GAUSSIAN_C.png)
![airplane_ADAPTIVE_THRESH_MEAN_C](zad2/airplane_ADAPTIVE_THRESH_MEAN_C.png)

![baboon_ADAPTIVE_THRESH_GAUSSIAN_C](zad2/baboon_ADAPTIVE_THRESH_GAUSSIAN_C.png)
![baboon_ADAPTIVE_THRESH_MEAN_C](zad2/baboon_ADAPTIVE_THRESH_MEAN_C.png)

![boats_ADAPTIVE_THRESH_GAUSSIAN_C](zad2/boats_ADAPTIVE_THRESH_GAUSSIAN_C.png)
![boats_ADAPTIVE_THRESH_MEAN_C](zad2/boats_ADAPTIVE_THRESH_MEAN_C.png)

### Otsu’s Binarization

![airplane_THRESH_BINARY+THRESH_OTSU](zad2/airplane_THRESH_BINARY+THRESH_OTSU.png)

![baboon_THRESH_BINARY+THRESH_OTSU](zad2/baboon_THRESH_BINARY+THRESH_OTSU.png)

![boats_THRESH_BINARY+THRESH_OTSU](zad2/boats_THRESH_BINARY+THRESH_OTSU.png)

