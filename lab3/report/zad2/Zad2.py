import cv2
import numpy as np
from matplotlib import pyplot as plt
import os

pathWrite = '/home/matej/osirv_20_21/lab3/report/zad2'
pathRead = '/home/matej/osirv_20_21/lab3/slike'

imgs = ['boats.bmp','baboon.bmp','airplane.bmp']

for img in imgs:
    image = cv2.imread(os.path.join(pathRead,img), 0)    
    
    ret,thresh1 = cv2.threshold(image,127,255,cv2.THRESH_BINARY)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_BINARY.png"), thresh1)
    ret,thresh2 = cv2.threshold(image,127,255,cv2.THRESH_BINARY_INV)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_BINARY_INV.png"), thresh2)
    ret,thresh3 = cv2.threshold(image,127,255,cv2.THRESH_TRUNC)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_TRUNC.png"), thresh3)
    ret,thresh4 = cv2.threshold(image,127,255,cv2.THRESH_TOZERO)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_TOZERO.png"), thresh4)
    ret,thresh5 = cv2.threshold(image,127,255,cv2.THRESH_TOZERO_INV)
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0]  + "_THRESH_TOZERO_INV.png"), thresh5)
     
    th2 = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(os.path.join(pathWrite, img.split('.')[0] + "_ADAPTIVE_THRESH_MEAN_C.png"), th2)
    th3 = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(os.path.join(pathWrite, img.split('.')[0] + "_ADAPTIVE_THRESH_GAUSSIAN_C.png"),th3)
    
    ret2,th4 = cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    cv2.imwrite(os.path.join(pathWrite, img.split('.')[0] + "_THRESH_BINARY+THRESH_OTSU.png"), th4)
    