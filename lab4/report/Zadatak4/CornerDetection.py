import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy
import os

pathWrite = '/home/matej/osirv_20_21/lab4/report/Zadatak4'
pathRead = '/home/matej/osirv_20_21/lab4/slike'

img = cv2.imread(os.path.join(pathRead,'chess.jpg'))
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
gray = np.float32(gray)

bs = [1,3,5]

for blockSize in bs:
    img2 = copy.copy(img)
    dst = cv2.cornerHarris(gray,blockSize,3,0.04)    
    #result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)    
    # threshold for an optimal value, it may vary depending on the image.
    img2[dst>0.01*dst.max()]=[0,0,255]
    cv2.imwrite(os.path.join(pathWrite,"chess_BlockSize" + str(blockSize) + ".jpg"), img2)