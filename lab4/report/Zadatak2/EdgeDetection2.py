import numpy as np
import argparse
import glob
import cv2
import os
from matplotlib import pyplot as plt

pathWrite = '/home/matej/osirv_20_21/lab4/report/Zadatak2'
pathRead = '/home/matej/osirv_20_21/lab3/slike'

imgs = ['airplane.bmp','barbara.bmp','boats.bmp','pepper.bmp']

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print("Median lower: %r." % lower)
        print("Median upper: %r." % upper)
        # return the edged image
        return edged

for img in imgs:
    image = cv2.imread(os.path.join(pathRead,img), 0)
    print(img)
    auto = auto_canny(image)    
    cv2.imwrite(os.path.join(pathWrite,img.split('.')[0] + "_autoCanny.jpg"), auto)
