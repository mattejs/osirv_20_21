import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
import os

pathWrite = '/home/matej/osirv_20_21/lab4/report/Zadatak5'
pathRead = '/home/matej/osirv_20_21/lab4/slike'
# Features Detection: 

img = cv2.imread(os.path.join(pathRead, 'roma_1.jpg'))
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert

orb = cv2.ORB_create()
key_points, description = orb.detectAndCompute(img, None)
#img_building_keypoints =cv2.drawKeypoints(img_building,key_points,img_building,flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
# rich keypoints: 
img_keypoints =cv2.drawKeypoints(img,key_points,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#Feature Extraction:
def image_detect_and_compute(detector, img_name):
    """Detect and compute intetrest points and their descriptors."""
    img = cv2.imread(os.path.join(pathRead, img_name))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = detector.detectAndCompute(img, None)
    return img, kp, des

def draw_image_matches(detector, img1_name, img2_name, nmatches=20):
    """Draw ORB feature matches of the given two images."""
    img1, kp1, des1 = image_detect_and_compute(detector, img1_name)
    img2, kp2, des2 = image_detect_and_compute(detector, img2_name)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key = lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, kp1, img2, kp2, matches[:nmatches],img2, flags=2)
#Draw Resultant Images:
    cv2.imwrite(os.path.join(pathWrite,"ORB_Interest_Point.jpg"), img_keypoints)
    cv2.imwrite(os.path.join(pathWrite,"Detector.jpg"), img_matches)

orb = cv2.ORB_create()
draw_image_matches(orb, 'roma_1.jpg','roma_2.jpg')
