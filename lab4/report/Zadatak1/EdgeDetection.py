import cv2
import numpy as np
from matplotlib import pyplot as plt
import os

pathWrite = '/home/matej/osirv_20_21/lab4/report/Zadatak1'
pathRead = '/home/matej/osirv_20_21/lab3/slike'

img = cv2.imread(os.path.join(pathRead,'lenna.bmp'), 0)

blurred = cv2.GaussianBlur(img, (5,5), 0)
edges1 = cv2.Canny(blurred,0,255)
cv2.imwrite(os.path.join(pathWrite,"lenna_0,255.jpg"), edges1)

edges2 = cv2.Canny(blurred,128,128)
cv2.imwrite(os.path.join(pathWrite,"lenna_128,128.jpg"), edges2)

edges3 = cv2.Canny(blurred,255,255)
cv2.imwrite(os.path.join(pathWrite,"lenna_255,255.jpg"), edges3)