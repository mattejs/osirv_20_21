import cv2
import numpy as np
import math
import copy
import os
from matplotlib import pyplot as plt

pathWrite = '/home/matej/osirv_20_21/lab4/report/Zadatak3'
pathRead = '/home/matej/osirv_20_21/lab4/slike'

img = cv2.imread(os.path.join(pathRead,'chess.jpg'))
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)

thetas = [90, 180]
thresholds = [150, 200]

for t in thetas:
    for th in thresholds:
        img2 = copy.copy(img)
        lines= cv2.HoughLines(edges, 1, math.pi/t, th, np.array([]), 0, 0)        
        a,b,c = lines.shape
        for i in range(a):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0, y0 = a*rho, b*rho
            pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
            pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
            cv2.line(img2, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)
        cv2.imwrite(os.path.join(pathWrite,"chess_" + str(t) + "," + str(th) + ".jpg"), img2)
