import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem6/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image = cv2.imread(os.path.join(pathRead, 'BoatsColor.bmp'), cv2.IMREAD_GRAYSCALE)

image = image.astype(np.float32)
image[image > 255] = 255
image[image < 0] = 0

for q in range(1,9):
    d = pow(2,8-q)
    imageFloat32 = image.copy()
    noise = np.random.uniform(0,1, imageFloat32.shape)
    imageFloat32 = (np.floor(imageFloat32 / d + noise) + 1/2) * d   
    imageInt8 = imageFloat32.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + 'n.bmp', imageInt8)

"""
Isto kao i u prethodnom problemu, za vrijednosti q > 4 kvaliteta slike se ne
mijenja značajnije, dok se za q = 4 utjecaj šuma više primjeti. Kada je q = 2 
i q = 3 dolazi do značajnijeg pada kvalitete, a za q = 1 je, isto kao u 
prethodnom problemu, teže raspoznati što je na slici.
"""
