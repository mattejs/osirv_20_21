import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem5/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image = cv2.imread(os.path.join(pathRead, 'BoatsColor.bmp'), cv2.IMREAD_GRAYSCALE)

image = image.astype(np.float32)
image[image > 255] = 255
image[image < 0] = 0

for q in range(1,9):
    d = pow(2,8-q)
    imageFloat32 = image.copy()
    imageFloat32 = (np.floor(imageFloat32 / d) + 1/2) * d   
    imageInt8 = imageFloat32.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', imageInt8)
    
"""
Kada je q > 4 razlika u slikama je jako mala, kod q = 4 može se primjetiti
pad kvalitete, najviše na dijelu slike gdje se nalazi nebo. Za q = 2 i q = 3
primjeti se značajniji pad kvalitete, ali se još uvijek može raspoznati što 
je na slici, dok je za q = 1 pad kvalitete toliko velik da je teško uopće 
raspoznati što je na slici
"""