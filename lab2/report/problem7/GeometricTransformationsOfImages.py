import numpy as np
import cv2
import os
from math import pi,cos,sin

pathWrite = 'C:/osirv_20_21/lab2/report/problem7/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image = cv2.imread(os.path.join(pathRead, 'baboon.bmp'), cv2.IMREAD_GRAYSCALE)
image = cv2.resize(image,None,fx=1/4, fy=1/4)
rows,cols = image.shape
images = []

for deg in range(0,360,30):
    M = cv2.getRotationMatrix2D((cols/2,rows/2),deg,1) 
    dst = cv2.warpAffine(image,M,(cols,rows))
    images.append(dst)

newImage = np.hstack(images)
cv2.imwrite('Baboon_rotated.bmp', newImage)



