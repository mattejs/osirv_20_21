import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem3/'
pathRead = 'C:/osirv_20_21/lab2/slike'

images = os.listdir(pathRead)
  
for image in images:
    imageGS = cv2.imread(os.path.join(pathRead, image), cv2.IMREAD_GRAYSCALE)
    cv2.imwrite(os.path.join(pathWrite , image.split('.')[0] + "_invert.png"), cv2.bitwise_not(imageGS))
    
