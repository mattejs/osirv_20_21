import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem4/'
pathRead = 'C:/osirv_20_21/lab2/slike'

images = os.listdir(pathRead)

thresholds = [63, 127, 191]

for image in images:
    imageGS = cv2.imread(os.path.join(pathRead, image), cv2.IMREAD_GRAYSCALE)
    for threshold in thresholds:
        th, imageTH = cv2.threshold(imageGS, threshold, 255, cv2.THRESH_TOZERO)        
        cv2.imwrite(image.split('.')[0] + '_' + str(threshold) + '_thresh.png', imageTH)

