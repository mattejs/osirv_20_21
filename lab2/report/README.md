# Problem 1 - Numpy basics

- Učitajte iz foldera ` images ` ( rootu repoa) tri različite slike, te ih spojite u jednu
sliku jednu do druge. Prikažite i spremite tu sliku. Koristiti funkcije `hstack` ili
`vstack`. Dozvoljeno je odsjeći dio slike kako bi svi dijelovi bili istih
dimenzija.

```python
import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem1/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image1 = cv2.imread(os.path.join(pathRead, 'barbara.bmp'))
image2 = cv2.imread(os.path.join(pathRead, 'goldhill.bmp'))
image3 = cv2.imread(os.path.join(pathRead, 'pepper.bmp'))

minHeight = np.min([image1.shape[0], image2.shape[0], image3.shape[0]])

image1 = image1[:minHeight]
image2 = image2[:minHeight]
image3 = image3[:minHeight]

hImage = np.hstack([image1, image2, image3])
cv2.imshow("Combined image", hImage)
cv2.imwrite(os.path.join(pathWrite , 'CombinedImage.jpg'), hImage)
cv2.waitKey(0)
cv2.destroyAllWindows()
``` 
![CombinedImage](https://gitlab.com/mattejs/osirv_20_21/-/blob/master/lab2/report/problem1/CombinedImage.jpg)

- Učitajte sliku i dodajte crni okvir oko slike, ali na način da ne
prepisujete dijelove slike, već da dodate okvir na rub slike. Kod za dodavanje
okvira stavite u funkciju i omogućite dodavanje proizvoljne širine okvira
(širina se predaje funkciji kao parametar).
```python
def Frame(image, frameWidth):        
    newImage = cv2.copyMakeBorder(
    image,
    top=frameWidth,
    bottom=frameWidth,
    left=frameWidth,
    right=frameWidth,
    borderType=cv2.BORDER_CONSTANT    
    )
    cv2.imshow("Combined image with frame", newImage)
    cv2.imwrite(os.path.join(pathWrite , 'CombinedImageFrame.jpg'), newImage)    

print("Enter frame width: ")
frameWidth = int(input())
Frame(hImage, frameWidth)

cv2.waitKey(0)
cv2.destroyAllWindows() 
``` 
![CombinedImageFrame](CombinedImageFrame.jpg)

# Problem 2 - Konvolucija

- Izvrtite konvoluciju na slici po izboru za primjere kernela sa [linka](https://en.wikipedia.org/wiki/Kernel_%28image_processing%29) .
- Kod i rezultantne slike dodajte u report.

```python
import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem2/'
pathRead = 'C:/osirv_20_21/lab2/slike'

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

image = cv2.imread(os.path.join(pathRead, 'goldhill.bmp'), cv2.IMREAD_GRAYSCALE)

cv2.imwrite(os.path.join(pathWrite , 'Identity.bmp'), convolve(image, np.array([[0,0,0],
                                                                                [0,1,0],
                                                                                [0,0,0]])))

cv2.imwrite(os.path.join(pathWrite , 'EdgeDetection1.bmp'), convolve(image, np.array([[1,0,-1],
                                                                                      [0,0,0],
                                                                                      [-1,0,1]])))

cv2.imwrite(os.path.join(pathWrite , 'EdgeDetection2.bmp'), convolve(image, np.array([[0,-1,0],
                                                                                      [-1,4,-1],
                                                                                      [0,-1,0]])))

cv2.imwrite(os.path.join(pathWrite , 'EdgeDetection3.bmp'), convolve(image, np.array([[-1,-1,-1],
                                                                                      [-1,8,-1],
                                                                                      [-1,-1,-1]])))

cv2.imwrite(os.path.join(pathWrite , 'Sharpen.bmp'), convolve(image, np.array([[0,-1,0],
                                                                               [-1,5,-1],
                                                                               [0,-1,0]])))

cv2.imwrite(os.path.join(pathWrite , 'BoxBlur.bmp'), convolve(image, 1/9*np.array([[1,1,1],
                                                                                   [1,1,1],
                                                                                   [1,1,1]])))

cv2.imwrite(os.path.join(pathWrite , 'GaussianBlur3x3.bmp'), convolve(image, 1/16*np.array([[1,2,1],
                                                                                            [2,4,2],
                                                                                            [1,2,1]])))


cv2.imwrite(os.path.join(pathWrite , 'GaussianBlur5x5.bmp'), convolve(image, 1/256*np.array([[1,4,6,4,1],
                                                                                             [4,16,24,16,4],
                                                                                             [6,24,36,24,6],
                                                                                             [4,16,24,16,4],
                                                                                             [1,4,6,4,1]])))

cv2.imwrite(os.path.join(pathWrite , 'UnsharpMasking5x5.bmp'), convolve(image, -1/256*np.array([[1,4,6,4,1],
                                                                                                [4,16,24,16,4],
                                                                                                [6,24,-476,24,6],
                                                                                                [4,16,24,16,4],
                                                                                                [1,4,6,4,1]])))
``` 

![Identity](Identity.bmp)
![EdgeDetection1](EdgeDetection1.bmp)
![EdgeDetection2](EdgeDetection2.bmp)
![EdgeDetection3](EdgeDetection3.bmp)
![Sharpen](Sharpen.bmp)
![BoxBlur](BoxBlur.bmp)
![GaussianBlur3x3](GaussianBlur3x3.bmp)
![GaussianBlur5x5](GaussianBlur5x5.bmp)
![UnsharpMasking5x5](UnsharpMasking5x5.bmp)

# Problem 3 - Invert

- Create a program which will load all images from `images` directory in grayscale 
- perform the invert operation and 
- save the images to `problem3/imagename_invert.png`.

```python
import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem3/'
pathRead = 'C:/osirv_20_21/lab2/slike'

images = os.listdir(pathRead)
  
for image in images:
    imageGS = cv2.imread(os.path.join(pathRead, image), cv2.IMREAD_GRAYSCALE)
    cv2.imwrite(os.path.join(pathWrite , image.split('.')[0] + "_invert.png"), cv2.bitwise_not(imageGS))
    
``` 

![airplane_invert](airplane_invert.png)
![baboon_invert](baboon_invert.png)
![barbara_invert](barbara_invert.png)
![boats_invert](boats_invert.png)
![BoatsColor_invert](BoatsColor_invert.png)
![dioslike_invert](dioslike_invert.png)
![goldhill_invert](goldhill_invert.png)
![hp_logo_invert](hp_logo_invert.png)
![lenna_invert](lenna_invert.png)
![pepper_invert](pepper_invert.png)
![pero_invert](pero_invert.png)

# Problem 4 - Threshold

- Create a program which will load all images from `images` directory
- perform the threshold operation with the parameter values of `63`, `127`, `191`
  - threshold operation should set all pixels with values `<= threshold` to 0 and leave
    the values `>= threshold` as they are.
- save the images to `problem4/imagename_parametervalue_thresh.png`

```python
import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem4/'
pathRead = 'C:/osirv_20_21/lab2/slike'

images = os.listdir(pathRead)

thresholds = [63, 127, 191]

for image in images:
    imageGS = cv2.imread(os.path.join(pathRead, image), cv2.IMREAD_GRAYSCALE)
    for threshold in thresholds:
        th, imageTH = cv2.threshold(imageGS, threshold, 255, cv2.THRESH_TOZERO)        
        cv2.imwrite(image.split('.')[0] + '_' + str(threshold) + '_thresh.png', imageTH)
``` 

![barbara_63_thresh](barbara_63_thresh.png)
![barbara_127_thresh](barbara_127_thresh.png)
![barbara_191_thresh](barbara_191_thresh.png)

# Problem 5 - Quantisation 

- Perform the quantisation on the image 
` BoatsColor.bmp` 
for all values of $` q `$ in the interval  $` [ 1, 8 ] `$ . 
- Save the images as
`.bmp` files with the filename format ` boats_q.bmp `,  where ` q ` is the
numeric value of $` q `$ used for that image.  
- Comment the visual quality of quantised images compared to original image, for
all  values of  $` q `$.

```python
import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem5/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image = cv2.imread(os.path.join(pathRead, 'BoatsColor.bmp'), cv2.IMREAD_GRAYSCALE)

image = image.astype(np.float32)
image[image > 255] = 255
image[image < 0] = 0

for q in range(1,9):
    d = pow(2,8-q)
    imageFloat32 = image.copy()
    imageFloat32 = (np.floor(imageFloat32 / d) + 1/2) * d   
    imageInt8 = imageFloat32.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', imageInt8)
    
"""
Kada je q > 4 razlika u slikama je jako mala, kod q = 4 može se primjetiti
pad kvalitete, najviše na dijelu slike gdje se nalazi nebo. Za q = 2 i q = 3
primjeti se značajniji pad kvalitete, ali se još uvijek može raspoznati što 
je na slici, dok je za q = 1 pad kvalitete toliko velik da je teško uopće 
raspoznati što je na slici
"""
``` 

![boats_1](boats_1.bmp)
![boats_2](boats_2.bmp)
![boats_3](boats_3.bmp)
![boats_4](boats_4.bmp)
![boats_5](boats_5.bmp)
![boats_6](boats_6.bmp)
![boats_7](boats_7.bmp)
![boats_8](boats_8.bmp)

# Problem 6 - Noise

- Perform the quantisation with the added noise, according to the formula
above. Use the same image and quantisation values as in Problem 3.
- Save the images as
`.bmp` files with the filename format ` boats_qn.bmp `,  where ` q ` is the
numeric value of $` q `$ used for that image.  
- Comment the visual quality of quantised images with noise compared to
quantised images in Problem 3, for all the values of $` q `$.

```python
import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem6/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image = cv2.imread(os.path.join(pathRead, 'BoatsColor.bmp'), cv2.IMREAD_GRAYSCALE)

image = image.astype(np.float32)
image[image > 255] = 255
image[image < 0] = 0

for q in range(1,9):
    d = pow(2,8-q)
    imageFloat32 = image.copy()
    noise = np.random.uniform(0,1, imageFloat32.shape)
    imageFloat32 = (np.floor(imageFloat32 / d + noise) + 1/2) * d   
    imageInt8 = imageFloat32.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + 'n.bmp', imageInt8)

"""
Isto kao i u prethodnom problemu, za vrijednosti q > 4 kvaliteta slike se ne
mijenja značajnije, dok se za q = 4 utjecaj šuma više primjeti. Kada je q = 2 
i q = 3 dolazi do značajnijeg pada kvalitete, a za q = 1 je, isto kao u 
prethodnom problemu, teže raspoznati što je na slici.
"""
``` 

![boats_1n](boats_1n.bmp)
![boats_2n](boats_2n.bmp)
![boats_3n](boats_3n.bmp)
![boats_4n](boats_4n.bmp)
![boats_5n](boats_5n.bmp)
![boats_6n](boats_6n.bmp)
![boats_7n](boats_7n.bmp)
![boats_8n](boats_8n.bmp)

# Problem 7 - Geometric transformations of Images

- Create a following 
[image](https://www.dropbox.com/s/5a7wxbk9zy782v3/4ex1.png?dl=1):
by iteratively rotating `baboon` image for 30 degrees between iterations, and
stacking results together. Before applying rotation to the image, scale the
image to quarter of its width and height.

```python
import numpy as np
import cv2
import os
from math import pi,cos,sin

pathWrite = 'C:/osirv_20_21/lab2/report/problem7/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image = cv2.imread(os.path.join(pathRead, 'baboon.bmp'), cv2.IMREAD_GRAYSCALE)
image = cv2.resize(image,None,fx=1/4, fy=1/4)
rows,cols = image.shape
images = []

for deg in range(0,360,30):
    M = cv2.getRotationMatrix2D((cols/2,rows/2),deg,1) 
    dst = cv2.warpAffine(image,M,(cols,rows))
    images.append(dst)

newImage = np.hstack(images)
cv2.imwrite('Baboon_rotated.bmp', newImage)
``` 

![Baboon_rotated](Baboon_rotated.bmp)