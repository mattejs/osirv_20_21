import numpy as np
import cv2
import os

pathWrite = 'C:/osirv_20_21/lab2/report/problem1/'
pathRead = 'C:/osirv_20_21/lab2/slike'

image1 = cv2.imread(os.path.join(pathRead, 'barbara.bmp'))
image2 = cv2.imread(os.path.join(pathRead, 'goldhill.bmp'))
image3 = cv2.imread(os.path.join(pathRead, 'pepper.bmp'))

minHeight = np.min([image1.shape[0], image2.shape[0], image3.shape[0]])

image1 = image1[:minHeight]
image2 = image2[:minHeight]
image3 = image3[:minHeight]

hImage = np.hstack([image1, image2, image3])
cv2.imshow("Combined image", hImage)
cv2.imwrite(os.path.join(pathWrite , 'CombinedImage.jpg'), hImage)
cv2.waitKey(0)
cv2.destroyAllWindows()

def Frame(image, frameWidth):        
    newImage = cv2.copyMakeBorder(
    image,
    top=frameWidth,
    bottom=frameWidth,
    left=frameWidth,
    right=frameWidth,
    borderType=cv2.BORDER_CONSTANT    
    )
    cv2.imshow("Combined image with frame", newImage)
    cv2.imwrite(os.path.join(pathWrite , 'CombinedImageFrame.jpg'), newImage)    

print("Enter frame width: ")
frameWidth = int(input())
Frame(hImage, frameWidth)

cv2.waitKey(0)
cv2.destroyAllWindows()


