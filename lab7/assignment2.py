import cv2 as cv

img = cv.imread("images/peppers.png")
# convert to HSV
hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

# TODO: Threshold img's hue range so that only green peppers are showing, and all other pixels are black
# Do this by thresholding a HSV image to green-yellow-ish hue range, and S and V components between 30 and 250.
# Use Google to find out which hue values to use for as the upper and lower bound, employ some trial and error
# to fine-tune the range so that you can see green peppers.
mask = cv.inRange(hsv, (25, 30, 30), (70, 250, 250))
img = cv.bitwise_and(img,img,mask=mask)

cv.imshow("Thresholded", img)
cv.waitKey(0)
cv.destroyAllWindows()
