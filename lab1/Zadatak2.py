import numpy as np
import cv2


path = "slike/baboon.bmp"
image = cv2.imread(path)

red = image.copy()
red[:,:,0] = red[:,:,1] = 0

green = image.copy()
green[:,:,0] = green[:,:,2] = 0

blue = image.copy()
blue[:,:,1] = blue[:,:,2] = 0

cv2.imwrite("rezultati/Red.jpg", red)
cv2.imwrite("rezultati/Green.jpg", green)
cv2.imwrite("rezultati/Blue.jpg", blue)

cv2.imshow("Red", red)
cv2.imshow("Green", green)
cv2.imshow("Blue", blue)
cv2.waitKey(0)
cv2.destroyAllWindows()


