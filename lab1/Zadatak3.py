import cv2

path = "slike/baboon.bmp"
image = cv2.imread(path)

newImage = cv2.copyMakeBorder(
                    image,
                    10, 10, 10, 10,
                    cv2.BORDER_CONSTANT
                )

cv2.imwrite("rezultati/ImageWithBorders.jpg", newImage)
