import cv2
 
cap = cv2.VideoCapture(0)
 
fourcc = cv2.VideoWriter_fourcc(*'XVID')
videoWriter = cv2.VideoWriter('camera.avi', fourcc, 30.0, (640,480))
 
while (True):
 
    ret, frame = cap.read()
     
    if ret:
        cv2.imshow('video', frame)
        videoWriter.write(frame)
 
    if cv2.waitKey(1) == 27:
        break
 
cap.release()
videoWriter.release() 
cv2.destroyAllWindows()